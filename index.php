<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <title>Nevera</title>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

        <link rel="stylesheet" href="assets/js/AdminLTE/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/js/AdminLTE/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/js/AdminLTE/Ionicons/css/ionicons.min.css" />
        <link rel="stylesheet" href="assets/css/AdminLTE/AdminLTE.min.css" />
        <link rel="stylesheet" href="assets/js/AdminLTE/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" href="assets/js/AdminLTE/datatables.net-bs/css/dataTables.bootstrap.min.css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="assets/css/AdminLTE/skins/_all-skins.min.css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="assets/js/vendor/ie/html5shiv.min.js"></script>
        <script src="assets/js/vendor/ie/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="assets/css/SourceSansPro.css" />
        <link rel="stylesheet" href="assets/css/styles.css" />
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <div class="content-wrapper no-margin">
                <section class="content-header">
                    <h1>
                        Despensa de la nevera
                        <?php // <small><a href="https://adminlte.io/themes/AdminLTE/index2.html" target="_blank">it all starts here</a></small> ?>
                    </h1>
                </section>

                <section class="content">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Listado</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success" title="Nuevo" data-toggle="modal" data-target="#modal-details" data-keyboard="true">Nuevo</button>
                            </div>
                        </div> <!-- /box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-hover" id="table-listado">
                                <thead>
                                    <tr>
                                        <th>Fecha de caducidad</th>
                                        <th>Fecha de entrada</th>
                                        <th>Nombre</th>
                                        <th>Cantidad</th>
                                        <th>Ubicaci&oacute;n</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div> <!-- /box-body -->
                        <div class="box-footer">
                        </div> <!-- /box-footer -->
                    </div>

                    <div id="modals">
                        <div class="modal fade" id="modal-details" tabindex="-1">
                            <form id="item">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title">A&ntilde;adir elemento</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="item-id" type="hidden" />

                                            <div class="form-group">
                                                <label for="item-name">Nombre</label>
                                                <input class="form-control" id="item-name" placeholder="Nombre" type="text" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <label for="item-cont">Cantidad</label>
                                                <div class="input-group">
                                                    <input class="form-control" id="item-cont" placeholder="Cantidad" type="number" required="required" min="1" />

                                                    <a class="input-group-addon" id="cont-sub" href="javascript:;" title="Restar unidad"><i class="fa fa-minus"></i></a>
                                                    <a class="input-group-addon" id="cont-add" href="javascript:;" title="Sumar unidad"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="item-location">Ubicaci&oacute;n</label>
                                                <select class="form-control" id="item-location">
                                                    <option value="1">Nevera</option>
                                                    <option value="2">Congelador</option>
                                                    <option value="3">Medicinas</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha de entrada</label>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <select class="form-control" id="item-entry_date_day">
                                                            <?php
                                                            foreach (range(0, 31) as $i) {
                                                                echo '<option value="' . sprintf('%02d', $i) . '">' . $i . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div> <!-- /dias -->
                                                    <div class="col-md-4">
                                                        <select class="form-control" id="item-entry_date_month">
                                                            <?php
                                                            foreach (range(0, 12) as $i) {
                                                                echo '<option value="' . sprintf('%02d', $i) . '">' . $i . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div> <!-- /meses -->
                                                    <div class="col-md-4">
                                                        <select class="form-control" id="item-entry_date_year">
                                                            <option value="0">0</option>
                                                            <?php
                                                            $year = date('Y');

                                                            foreach (range($year, ($year + 10)) as $i) {
                                                                echo '<option value="' . $i . '">' . $i . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div> <!-- /anyos -->
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha de caducidad</label>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <select class="form-control" id="item-expiration_date_day">
                                                            <?php
                                                            foreach (range(0, 31) as $i) {
                                                                echo '<option value="' . sprintf('%02d', $i) . '">' . $i . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div> <!-- /dias -->
                                                    <div class="col-md-4">
                                                        <select class="form-control" id="item-expiration_date_month">
                                                            <?php
                                                            foreach (range(0, 12) as $i) {
                                                                echo '<option value="' . sprintf('%02d', $i) . '">' . $i . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div> <!-- /meses -->
                                                    <div class="col-md-4">
                                                        <div class="input-group">
                                                            <select class="form-control" id="item-expiration_date_year">
                                                                <option value="0">0</option>
                                                                <?php
                                                                $year = date('Y');

                                                                foreach (range($year, ($year + 10)) as $i) {
                                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                                }
                                                                ?>
                                                            </select>

                                                            <a class="input-group-addon" id="item-expiration_date_remove" href="javascript:;" title="Sin fecha de caducidad"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div> <!-- /anyos -->
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div> <!-- /modal-body -->
                                        <div class="modal-footer">
                                            <button class="btn btn-default pull-left" type="button" data-dismiss="modal" title="Cerrar">Cerrar</button>
                                            <button class="btn btn-primary" id="btn-submit" type="submit" title="" data-new="A&ntilde;adir" data-edit="Editar"></button>
                                        </div> <!-- /modal-footer -->
                                    </div> <!-- /modal-content -->
                                </div> <!-- /modal-dialog -->
                            </form>
                        </div> <!-- /modal-details -->

                        <div class="modal fade" id="modal-delete">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title">Eliminar elemento</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input id="item-id-delete" type="hidden" />

                                        <p>&iquest;Estás seguro de que quieres eliminar este elemento?</p>
                                    </div> <!-- /modal-body -->
                                    <div class="modal-footer">
                                        <button class="btn btn-default pull-left" type="button" data-dismiss="modal" title="Cerrar">Cerrar</button>
                                        <button class="btn btn-primary" id="btn-delete" type="submit" title="Eliminar" data-new="Eliminar">Eliminar</button>
                                    </div> <!-- /modal-footer -->
                                </div> <!-- /modal-content -->
                            </div> <!-- /modal-dialog -->
                        </div> <!-- /modal-delete -->
                    </div> <!-- /modals -->
                </section>
            </div>

            <!--<footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
            </footer>-->
        </div>

        <script type="text/javascript" src="assets/js/AdminLTE/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/fastclick/lib/fastclick.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/adminlte.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/datatables.net/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/js/AdminLTE/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/nevera.js"></script>
    </body>
</html>
