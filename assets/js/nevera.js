/**
 * Configuramos el selector de fecha
 */
//function setDatepicker() {
//    $('.datepicker').datepicker({
//        format: 'yyyy-mm-dd',
//        language: 'es'
//    });
//}
//;

/**
 * Configuramos la tabla del listado
 */
function setDataTable() {
    $('#table-listado').DataTable({
        'language': {
            'url': 'assets/js/AdminLTE/datatables.net/js/lang/es.json'
        },
        'ordering': false,
        'paging': false
    });
}
;

/**
 * Cambia el valor de la cantidad total
 */
function subAddCont() {
    $('#cont-sub').on('click', function () {
        var val = parseInt($('#modal-details #item-cont').val());

        if (!val) {
            val = 2;
        }

        if (1 < val) {
            $('#modal-details #item-cont').val(val - 1);
        }
    });

    $('#cont-add').on('click', function () {
        var val = parseInt($('#modal-details #item-cont').val());

        if (!val) {
            val = 0;
        }

        $('#modal-details #item-cont').val(val + 1);
    });
}
;

/**
 * Limpiamos los input/select, y si hemos venido del boton editar, rellenamos los campos con los datos
 */
function populateModal() {
    $('#modal-details').on('shown.bs.modal', function (e) {
        // Vaciamos el contenido de los campos
        $(this).find('input').each(function () {
            $(this).val('');
        });

        // Marcamos la primera opcion de los select
        $(this).find('select').each(function () {
            $('#' + $(this).attr('id')).val($('#' + $(this).attr('id') + ' option:first').val());
        });

        // Ponemos el texto correspondiente la boton
        $(this).find('#btn-submit').html($(this).find('#btn-submit').attr('data-new'));
        $(this).find('#btn-submit').attr('title', $(this).find('#btn-submit').attr('data-new'));

        var idItem = $(e.relatedTarget).data('id-item');

        // Si venimos de editar rellenamos los campos
        if (idItem) {
            $(this).find('#btn-submit').html($(this).find('#btn-submit').attr('data-edit'));
            $(this).find('#btn-submit').attr('title', $(this).find('#btn-submit').attr('data-edit'));

            $('#item-id').val(idItem);
            $('#item-name').val($('#item_' + idItem + ' [data-name]').attr('data-name'));
            $('#item-cont').val($('#item_' + idItem + ' [data-cont]').attr('data-cont'));

            $('#item-location').val($('#item-location option[value="' + $('#item_' + idItem + ' [data-location]').attr('data-location') + '"]').val());

            var entryDate = new Date($('#item_' + idItem + ' [data-entry_date]').attr('data-entry_date'));

            var year = '0';
            var month = '00';
            var day = '00';

            if ('Invalid Date' != entryDate) {
                year = entryDate.getFullYear();
                month = ('0' + (entryDate.getMonth() + 1)).slice(-2);
                day = ('0' + entryDate.getDate()).slice(-2);
            }

            $('#item-entry_date_year').val($('#item-entry_date_year option[value="' + year + '"]').val());
            $('#item-entry_date_month').val($('#item-entry_date_month option[value="' + month + '"]').val());
            $('#item-entry_date_day').val($('#item-entry_date_day option[value="' + day + '"]').val());

            var expirationDate = new Date($('#item_' + idItem + ' [data-expiration_date]').attr('data-expiration_date'));

            year = '0';
            month = '00';
            day = '00';

            if ('Invalid Date' != expirationDate) {
                year = expirationDate.getFullYear();
                month = ('0' + (expirationDate.getMonth() + 1)).slice(-2);
                day = ('0' + expirationDate.getDate()).slice(-2);
            }

            $('#item-expiration_date_year').val($('#item-expiration_date_year option[value="' + year + '"]').val());
            $('#item-expiration_date_month').val($('#item-expiration_date_month option[value="' + month + '"]').val());
            $('#item-expiration_date_day').val($('#item-expiration_date_day option[value="' + day + '"]').val());
        }
    });

    $('#modal-delete').on('shown.bs.modal', function (e) {
        $('#item-id-delete').val($(e.relatedTarget).data('id-item'));
    });
}
;

/**
 * Vaciamos la fecha de caducidad
 */
function removeExpirationDate() {
    $('#item-expiration_date_remove').on('click', function () {
        $('#item-expiration_date_day').val('00');
        $('#item-expiration_date_month').val('00');
        $('#item-expiration_date_year').val('0');
    });
}
;

/**
 * Devovlemos todos los registros de la BBDD y los pintamos en la tabla
 */
function getResults() {
    $('html').addClass('cursor-wait');

    $.ajax({
        url: 'classes/getAll.php',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                if (0 < data.data.length) {
                    $('#table-listado').DataTable().clear();

                    var oneDay = 24 * 60 * 60 * 1000;
                    var today = new Date();

                    $.each(data.data, function (key, value) {
//                        rows += '<tr id="item_' + value.id + '">';
//                        rows += '    <td data-name="' + value.name + '">' + value.name.charAt(0).toUpperCase() + value.name.slice(1) + '</td>';
//                        rows += '    <td data-cont="' + value.cont + '">' + value.cont + '</td>';
//                        rows += '    <td data-location="' + value.location + '">' + value.location.charAt(0).toUpperCase() + value.location.slice(1) + '</td>';
//                        rows += '    <td data-expiration_date="' + value.expiration_date + '">' + value.expiration_date + '</td>';
//                        rows += '    <td>';
//                        rows += '        <button type="button" class="btn btn-success" title="Editar" data-toggle="modal" data-target="#modal-details" data-id-item="' + value.id + '"><i class="fa fa-edit"></i></button>';
//                        rows += '        <button type="button" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#modal-delete" data-id-item="' + value.id + '"><i class="fa fa-trash"></i></button>';
//                        rows += '    </td>';
//                        rows += '</tr>';

                        var entryDate = new Date(value.entry_date);
                        entryDate = entryDate.getFullYear() + '-' + ('0' + (entryDate.getMonth() + 1)).slice(-2) + '-' + ('0' + entryDate.getDate()).slice(-2);

                        var expirationDate = new Date(value.expiration_date);
                        var originalExpirationDate = expirationDate;

                        if (isNaN(expirationDate.getTime())) {
                            expirationDate = '';
                        } else {
                            expirationDate = expirationDate.getFullYear() + '-' + ('0' + (expirationDate.getMonth() + 1)).slice(-2) + '-' + ('0' + expirationDate.getDate()).slice(-2);
                        }

                        var row = $('#table-listado').DataTable().row.add([
                            '<span data-expiration_date="' + expirationDate + '">' + expirationDate + '</span>',
                            '<span data-entry_date="' + entryDate + '">' + entryDate + '</span>',
                            '<span data-name="' + value.name + '">' + value.name + '</span>',
                            '<span data-cont="' + value.cont + '">' + value.cont + '</span>',
                            '<span data-location="' + value.location + '">' + $('#item-location option[value=' + value.location + ']').text() + '</span>',
                            '<button type="button" class="btn btn-success" title="Editar" data-toggle="modal" data-target="#modal-details" data-id-item="' + value.id + '"><i class="fa fa-edit"></i></button> ' + ' <button type="button" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#modal-delete" data-id-item="' + value.id + '"><i class="fa fa-trash"></i></button>'
                        ]).node().id = 'item_' + value.id;

                        $('#table-listado').DataTable().draw(false);

                        var diffDays = Math.round((originalExpirationDate - today) / oneDay);
                        diffDays = isNaN(diffDays) ? 1000 : diffDays;

                        // Agregamos las clases danger o warning dependiendo de los dias que queden o hayan pasado
                        if (1 >= diffDays) {
                            $('#' + row).addClass('bg-danger');
                        } else if (3 >= diffDays) {
                            $('#' + row).addClass('bg-warning');
                        }
                    });
                }
            }
        }
    })
            .fail(function (data) {
                if (data.responseJSON) {
                    alert(data.responseJSON.message);
                } else {
                    alert(data.statusText);
                }
            })
            .always(function (jqXHR, textStatus) {
                $('html').removeClass('cursor-wait');
            });
}
;

/**
 * Guardamos el item, sea nuevo o editar
 */
function setItem() {
    $('#item').on('submit', function (e) {
        e.preventDefault();

        var data = {
            id: $.trim($('#item-id').val()),
            name: $.trim($('#item-name').val()),
            cont: $.trim($('#item-cont').val()),
            location: $.trim($('#item-location').val()),
            entry_date_day: $.trim($('#item-entry_date_day').val()),
            entry_date_month: $.trim($('#item-entry_date_month').val()),
            entry_date_year: $.trim($('#item-entry_date_year').val()),
            expiration_date_day: $.trim($('#item-expiration_date_day').val()),
            expiration_date_month: $.trim($('#item-expiration_date_month').val()),
            expiration_date_year: $.trim($('#item-expiration_date_year').val())
        };

        $('html').addClass('cursor-wait');

        $.ajax({
            url: 'classes/setItem.php',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    // Actualizamos la tabla
                    getResults();

                    $('#modal-details').modal('toggle');
                } else {
                    alert(data.message);
                }
            }
        })
                .fail(function (data) {
                    if (data.responseJSON) {
                        alert(data.responseJSON.message);
                    } else {
                        alert('Error!');
                    }
                })
                .always(function (jqXHR, textStatus) {
                    $('html').removeClass('cursor-wait');
                });
    });
}
;

/**
 * Eliminamos un item
 */
function deleteItem() {
    $('#btn-delete').on('click', function (e) {
        e.preventDefault();

        var data = {
            id: $('#item-id-delete').val()
        };

        $('html').addClass('cursor-wait');

        $.ajax({
            url: 'classes/deleteItem.php',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    // Actualizamos la tabla
                    getResults();
                } else {
                    alert(data.message);
                }
            }
        })
                .fail(function (data) {
                    alert(data.responseJSON.message);
                })
                .always(function (jqXHR, textStatus) {
                    $('html').removeClass('cursor-wait');

                    $('#modal-delete').modal('toggle');
                });
    });
}
;

$(document).ready(function () {
//    setDatepicker();

    setDataTable();

    subAddCont();

    removeExpirationDate();

    populateModal();

    getResults();

    setItem();

    deleteItem();
});