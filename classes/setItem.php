<?php

include_once __DIR__ . '/db/bbdd.php';

$resultado = null;

try {
    foreach (['name', 'cont', 'location', 'entry_date_year', 'entry_date_month', 'entry_date_day'] as $requiredParam) {
        if (!isset($_POST[$requiredParam]) || empty($_POST[$requiredParam])) {
            throw new \Exception('El campo ' . $requiredParam . ' es obligatorio');
        }
    }

    $entryDate = new \DateTime(date($_POST['entry_date_year'] . '-' . $_POST['entry_date_month'] . '-' . $_POST['entry_date_day']));
    $entryDate = $entryDate->format('Y-m-d 00:00:00');

    $expirationDate = new \DateTime(date($_POST['expiration_date_year'] . '-' . $_POST['expiration_date_month'] . '-' . $_POST['expiration_date_day']));
    $expirationDateYear = $expirationDate->format('Y');
    $expirationDate = $expirationDate->format('Y-m-d 00:00:00');

    // Si el año es inferior al año actual ponemos el campo a null
    if (date('Y') > $expirationDateYear) {
        $expirationDate = null;
    }

    if ('' !== $_POST['id'] && is_numeric($_POST['id'])) {
        $sql = 'UPDATE ' . $table . ' SET name = "' . $_POST['name'] . '", cont = ' . $_POST['cont'] . ', location = "' . $_POST['location'] . '", entry_date="' . $entryDate . '" ,expiration_date = "' . $expirationDate . '" WHERE id = ' . $_POST['id'];
    } else {
        $sql = 'INSERT INTO ' . $table . ' (name, cont, location, entry_date, expiration_date) VALUES ("' . $_POST['name'] . '", ' . $_POST['cont'] . ', "' . $_POST['location'] . '", "' . $entryDate . '", "' . $expirationDate . '")';
    }

    $resultado = mysqli_query($conn, $sql);

    $error = $conn->error;
} catch (\Exception $e) {
    $error = $e->getMessage();
}


if ($resultado) {
    header('HTTP/1.1 200');

    echo json_encode(['success' => true, 'data' => [], 'message' => 'Ok']);
} else {
    header('HTTP/1.1 500 Internal Server Error');

    echo json_encode(['success' => false, 'data' => [], 'message' => 'Error: ' . $error]);
}

die;
