<?php

include_once __DIR__ . '/db/bbdd.php';

$sql = 'SELECT * FROM ' . $table . ' WHERE expiration_date < DATE_ADD(NOW(), INTERVAL 7 DAY) ORDER BY expiration_date ASC;';

$resultado = mysqli_query($conn, $sql);

if ($resultado) {
    $rows = mysqli_fetch_all($resultado, MYSQLI_ASSOC);

    // Si se han encontrado registros que cumplen la condicion mandamos un correo
    if (0 < count($rows)) {
        $message = 'Listado de articulos que estan a punto de caducar:
            ';

        foreach ($rows as $row) {
            $message .= $row['name'] . '. Cantidad: ' . $row['cont'] . '. Fecha de caducidad: ' . $row['expiration_date'] . '
            ';
        }

        $headers = 'MIME-Version: 1.0' . '\r\n';
        $headers .= 'Content-type:text/html;charset=UTF-8' . '\r\n';
        $headers .= 'From: tankerai@gmail.com' . '\r\n' .
                'Reply-To: tankerai@gmail.com' . '\r\n' .
                'X-Mailer: PHP/' . phpversion();

        mail('tankerai@gmail.com,ziruagr@gmail.com', 'Mi nevera - A punto de caducar', $message, $headers);
    }
}

die;
