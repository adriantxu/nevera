<?php

include_once __DIR__ . '/db/bbdd.php';

$sql = 'DELETE FROM ' . $table . ' WHERE id = ' . $_POST['id'];

$resultado = mysqli_query($conn, $sql);

if ($resultado) {
    header('HTTP/1.1 200');

    echo json_encode(['success' => true, 'data' => [], 'message' => 'Ok']);
} else {
    header('HTTP/1.1 500 Internal Server Error');

    echo json_encode(['success' => false, 'data' => [], 'message' => 'Error: ' . $conn->error]);
}

die;
