<?php

include_once __DIR__ . '/db/bbdd.php';

$resultado = mysqli_query($conn, 'SELECT * FROM ' . $table . ' ORDER BY expiration_date ASC, entry_date ASC, cont ASC, name ASC, location ASC');

if ($resultado) {
    $rows = mysqli_fetch_all($resultado, MYSQLI_ASSOC);

    header('HTTP/1.1 200');

    echo json_encode(['success' => true, 'data' => $rows, 'message' => 'Ok']);
} else {
    header('HTTP/1.1 500 Internal Server Error');

    echo json_encode(['success' => false, 'data' => [], 'message' => 'Error: ' . $conn->error]);
}

die;